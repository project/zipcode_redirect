<?php

/**
 * @file
 * Admin page callbacks for zipcode redirect module
 */
 
/**
 * Form builder; The settings form for zipcode redirect
 *
 * @ingroup forms
 */
function zipcode_redirect_settings($instance = 1) {
  $form = array('#prefix' => '<p>'. t('Use this form to add multiple zipcodes and a distance in miles from zip code to test.  Also add the URL to redirect to if the zipcode search is true and false. Only the last FALSE URL needs to be set.') .'</p>');
  $form['units'] = array(
    '#type' => 'fieldset',
    '#title' => t('Unit Measurement'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['units']['zipcode_redirect_units'] = array(
    '#type' => 'radios',
    '#description' => t('Choose the type of units to measure distance. (Miles or Kilometers)'),
    '#options' => array('mi', 'km'),
    '#default_value' => variable_get('zipcode_redirect_units', 'mi'),
  );
  
    
  $zipcode_values = variable_get('zipcode_redirect_zips', array());
  $zipcodes_count = !empty($zipcode_values) ? count($zipcode_values) : 1;
  
  // Loop through the number of zipcodes saved
  for ($delta = 0; $delta < $zipcodes_count; $delta++) {
    $form['zipcodes'][$delta] = _zipcode_redirect_settings_fields($delta);
    
    _zipcode_redirect_ensure_false($form);
  }
  
  // Container for addittional zipcodes
  $form['zipcode_redirect_zips'] = array(
    '#value' => ' ',
    '#prefix' => '<div id="more-zipcodes">',
    '#suffix' => '</div>',
  );
  
  $form['add_another'] = array(
    '#type' => 'button', 
    '#value' => t('Add Another Zipcode'),
    '#ahah' => array(
      'event' => 'click',
      'path' => 'zipcode_redirect/addanother_js',
      'wrapper' => 'more-zipcodes',
      'method' => 'replace',
      'effect' => 'fade'
    ),
  );
    
  return system_settings_form($form);
}

function zipcode_redirect_settings_validate(&$form, &$form_state) {
  // Remove any rows where all four fields are blank
  // TODO: Add custom validation when only a couple fields are filled in
  foreach ($form_state['values']['zipcode_redirect_zips'] as $key => $zip) {
    if ($zip['zr_zipcode'] == '' && $zip['zr_radius'] == '' && $zip['zr_true'] == '') {
      unset($form_state['values']['zipcode_redirect_zips'][$key]);
    }
  }
}

/**
* Only the last False URL will ever be used so we want to disable any previous False URL if exists
*/
function _zipcode_redirect_ensure_false(&$form) {
  $total = count($form['zipcodes']);
  for ($i=0; $i < $total - 1; $i++) {
    if (is_array($form['zipcodes'][$i]) && is_array($form['zipcodes'][$i]['zr_false'])) {
      $form['zipcodes'][$i]['zr_false']['#default_value'] = '';
      $form['zipcodes'][$i]['zr_false']['#disabled'] = TRUE;
    }
  }
  
}

/**
 * Form Fields for settings screen
 * In separate function so that I can add more fields via AHAH
 */
function _zipcode_redirect_settings_fields($delta) {
  $values = variable_get('zipcode_redirect_zips', array());
  $form['#tree'] = TRUE;
  
  $form['zr_zipcode'] = array(
    '#type' => 'textfield',
    '#title' => t('Zipcode @n', array('@n' => ($delta + 1))),
    '#description' => t('Base Zipcode'),
    '#size' => 10,
    '#maxlength' => 255,
    '#default_value' => $values[$delta]['zr_zipcode'],
    '#parents' => array('zipcode_redirect_zips', $delta, 'zr_zipcode'),
  );
  
  $form['zr_radius'] = array(
    '#type' => 'textfield',
    '#title' => t('Distance @n', array('@n' => ($delta + 1))),
    '#description' => t('Radius'),
    '#size' => 5,
    '#maxlength' => 255,
    '#default_value' => $values[$delta]['zr_radius'],
    '#parents' => array('zipcode_redirect_zips', $delta, 'zr_radius'),
  );
  
  $form['zr_true'] = array(
    '#type' => 'textfield',
    '#title' => t('True URL @n', array('@n' => ($delta + 1))),
    '#description' => t('If the zip code falls within the radius'),
    '#size' => 25,
    '#maxlength' => 255,
    '#default_value' => $values[$delta]['zr_true'],
    '#parents' => array('zipcode_redirect_zips', $delta, 'zr_true'),
  );
  
  $form['zr_false'] = array(
    '#type' => 'textfield',
    '#title' => t('False URL @n', array('@n' => ($delta + 1))),
    '#description' => t('If the zip code falls outside the radius'),
    '#size' => 25,
    '#maxlength' => 255,
    '#default_value' => $values[$delta]['zr_false'],
    '#parents' => array('zipcode_redirect_zips', $delta, 'zr_false'),
  );
  
  $form['divider'] = array(
    '#value' => "<hr class='clear' />",
  );
  
  
  return $form;
}

/**
 * Menu callback for adding another zipcode via AHAH
 */
function zipcode_redirect_add_fields_js() {
  $delta = count($_POST['zipcode_redirect_zips']);
  
  // Build the new form element.
  $form_element = _zipcode_redirect_settings_fields($delta);
  drupal_alter('form', $form_element, array(), 'zipcode_redirect_add_fields_js');
  
  // Build the new form
  $form_state = array('submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  // Add the new element to the stored form. Without adding the element to the
  // form, Drupal is not aware of this new elements existence and will not
  // process it. We retreive the cached form, add the element, and resave.
  if (!$form = form_get_cache($form_build_id, $form_state)) {
    exit();
  }
  $form['zips'][$delta] = $form_element; 
  form_set_cache($form_build_id, $form, $form_state);
  $form += array(
    '#post' => $_POST,
    '#programmed' => FALSE,
  );

  // Rebuild the form.
  $form = form_builder('zipcode_redirect_settings', $form, $form_state);
  
  // Render the new output.
  $zips_form = $form['zips'];
  unset($zips_form['#prefix'], $zips_form['#suffix']); // Prevent duplicate wrappers.
  $zips_form[$delta]['#attributes']['class'] = empty($zips_form[$delta]['#attributes']['class']) ? 'ahah-new-content' : $choice_form[$delta]['#attributes']['class'] .' ahah-new-content';
  $output = theme('status_messages') . drupal_render($zips_form);

  drupal_json(array('status' => TRUE, 'data' => $output));
}

/**
 * Implementation of hook_form_alter().
 */
function zipcode_redirect_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'zipcode-redirect-settings') {
    $form['#validate'][] = 'zipcode_redirect_settings_validate';
    _zipcode_redirect_ensure_false(&$form);
  }
}